import axios from 'axios'
let form = {

  data () {
    return {
      api: {
        url: 'https://vevanta.ru/api/mail/vevanta'
      },
      formName: '',
      msg: '',
      inputs: {
        phone: '',
        email: '',
        name: ''
      },
      dataRequest: {}
    }
  },

  methods: {
    sendForm () {
      if (this.inputs.phone || this.inputs.email) {
        let msg = this.msg
        this.msg = `Форма: ${this.formName} <br>Комментарий: ${msg}`

        let dataRequest = {
          ...this.inputs,
          ...this.dataRequest,
          name: this.inputs.name,
          form: this.formName,
          msg: this.msg,
          phone: this.inputs.phone,
          email: this.inputs.email,
          url: location.href
        }

        axios.post(this.api.url, dataRequest)
          .then(() => {
            this.inputs.phone = ''
            this.inputs.email = ''
            this.inputs.name = ''
            this.msg = ''
          })
      }
    }
  }
}

export default form
